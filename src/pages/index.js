import { useLayoutEffect, useState } from "react";
import Header from "./components/Header";

export default function Home() {
  // Username and message
  const [sender, setSender] = useState("");
  const [chatMessage, setChatMessage] = useState("");
  // openapi key and result 
  const [summary, setSummary] = useState("Summary will be outputted here");
  const [apiKey, setApiKey] = useState("");
  // messages lists string and div
  const [messages, setMessages] = useState([]);
  const [msgList, setMsgList] = useState([]);
  // states for option parameters
  const [model, setModel] = useState("text-davinci-003");
  const [maxTokens, setMaxTokens] = useState(50);
  const [tort, setTort] = useState("temperature");
  const [temperature, setTemperature] = useState(0.7);
  const [top_p, setTop_p] = useState(1);

  const handleDeleteMessage = (index) => {
    setMessages(oldValues => {
      return oldValues.filter((_, i) => i !== index)
    })
    setMsgList(oldValues => {
      return oldValues.filter((_, i) => i !== index)
    })
  };  

  const handleChat = (ev) => {
    ev.preventDefault();
    if(!chatMessage || messages.length == 10) return;
    if(chatMessage === "") return;
    let msg = "";
    if(messages.length % 2 == 0){
      msg = "Suzy: " + chatMessage;
    }
    else {
      msg = "Cathy: " + chatMessage;
    }
    if(sender !== "") msg = sender + ": " + chatMessage;
    const newMessage = <div key={messages.length} className="description" style={{cursor : "pointer"}} onClick={() => handleDeleteMessage(messages.length)}>{msg}</div>
    
    // update states
    setChatMessage("");
    setMessages([...messages, newMessage]);
    setMsgList([...msgList, msg]);
  };

  const getSummary = async () => {
    let messagesString = "";
    for(let i = 0; i < msgList.length; i++){
      messagesString += msgList[i] + " ";
    }
    const response = await fetch("/api/endpoint", {
      method : "POST",
      body : JSON.stringify({
        "messagesString" : messagesString, 
        "apiKey" : apiKey,
        "model" : model,
        "maxTokens" : maxTokens,
        "tort" : tort,
        "temperature" : temperature,
        "top_p" : top_p,
      }),
      apiKey : JSON.stringify(apiKey),
    }).then((response) => response.json())
    .then((data) => {
      setSummary(data.result);
    });
  }
/* eslint-disable */

  return (
    <>
      <center>
        <Header />
        <div className="description">
          This is a web application that uses OpenAI's API to summarize chat messages. Chat messages are parsed as a string with the origin&apos;s name as well as the contents of the chat message. The result is summarized upon request.
        </div>
        <br />
        <div className="description">To use this web application, enter up to 10 chat messages. A username is OPTIONAL. Messages can be deleted if you click on them. One you are ready to summarize the messages, feel free to enter your OpenAI API key as well as the sumamrize button to summarize the messages.</div>
        <br />
        <div className="description">Model {"-->"} select any model available; models are ranked from most powerful to least</div>
        <br />
        <div className="description">Max Tokens {"-->"} limits the number of tokens that you want to use when generating a response</div>
        <br />
        <div className="description">Temperature {"-->"} variance and randomness of a response</div>
        <br />
        <div className="description">top_p {"-->"} alternative to sampling with temperature, called nucleus sampling, where the model considers the results of the tokens with top_p probability mass</div>
      </center>
      <hr />
      <center>
        <div className="chat-box">
          {messages}
        </div>
        <form onSubmit={handleChat}>
          <input type="text" value={sender} onChange={ev => setSender(ev.target.value)} placeholder="Username"/>
          <input type="text" value={chatMessage} onChange={ev => setChatMessage(ev.target.value)} placeholder="Chat Message"/>
          <br />
          <button style={{marginTop : "10px"}} type="submit" >Submit Chat</button>
        </form>
        <hr />
        <p className="summary">{summary}</p>
        <button style={{marginTop : "10px"}} onClick={() => getSummary()} >Request Summary</button>
        <hr />
        <div className="options mt">
          <div className="white-text">Please select any options to tweak the AI</div>
          <select value={model} onChange={ev => setModel(ev.target.value)} className="mt">
            <option value="text-davinci-003">text-davinci-003</option>
            <option value="text-curie-001">text-curie-001</option>
            <option value="text-babbage-001">text-babbage-001</option>
            <option value="text-ada-001">text-ada-001</option>
          </select>
          <div className="flex-container mt">
            <div className="white-text">Max Tokens: </div>
            <input type="range" min="10" max="150" value={maxTokens} onChange={ev => setMaxTokens(ev.target.value)}/>
            <div className="white-text">{'(' + maxTokens + ')'}</div>
          </div>
          <select value={tort} onChange={ev => setTort(ev.target.value)} className="mt">
            <option value="temperature">temperature</option>
            <option value="top_p">top_p</option>
          </select>
          {tort === "temperature" && (
          <div className="flex-container mt">
            <div className="white-text">Temperature: </div>
            <input type="range" min="0.1" max="2" step="0.1" value={temperature} onChange={ev => setTemperature(ev.target.value)}/>
            <div className="white-text">{'(' + temperature + ')'}</div>
          </div>)}
          {tort === "top_p" && (
            <div className="flex-container mt">
            <div className="white-text">top_p: </div>
            <input type="range" min="0.1" max="1" step="0.1" value={top_p} onChange={ev => setTop_p(ev.target.value)}/>
            <div className="white-text">{'(' + top_p + ')'}</div>
          </div>
          )}
        </div>
        <hr />
        <input type="text" value={apiKey} onChange={ev => setApiKey(ev.target.value)} placeholder={"Enter API Key"} />
      </center>
    </>
  )
}
/* eslint-enable */
