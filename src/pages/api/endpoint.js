// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { Configuration, OpenAIApi } from "openai";

var key = "";

export default async function handler(req, res) {
  if (req.method === 'POST') {
    // Handle POST request here
    const conversion = JSON.parse(req.body);
    const requestBody = conversion.messagesString;
    if(conversion.apiKey !== "") key = conversion.apiKey;
  
    const configuration = new Configuration({
      apiKey: key,
    });
    const openai = new OpenAIApi(configuration);

    const mdl = conversion.model;
    const mt = parseInt(conversion.maxTokens);
    const tort = conversion.tort;
    const tmp = parseFloat(conversion.temperature);
    const tpp = parseFloat(conversion.top_p);

    var completion = {};

    if(tort === "temperature"){
      completion = await openai.createCompletion({
        model: mdl,
        prompt: `${requestBody} summarize the conversation:`,
        temperature: tmp,
        max_tokens: mt,
      });
    }
    else{
      completion = await openai.createCompletion({
        model: mdl,
        prompt: `${requestBody} summarize the conversation:`,
        top_p: tpp,
        max_tokens: mt,
      });
    }
    
    var str = completion.data.choices[0].text;


    res.json({
      result : str,
    });
  } else {
    res.status(405).json({ message: 'Method not allowed' });
  }
}
